﻿Shader "Custom/Teeth"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Point ("Point", vector) = (0,0,0,0)
        _AOE ("Area of Effect", float) = 4
        _NoiseRes ("Noise Res", float) = 4
        _NoiseIntensification ("Noise Intensification", Range(1,8)) = 2
        _EffectScale ("Effect Scale", float) = 1
        [Toggle] _UseTime ("Use Time", float) = 1
        _NoiseOffset ("Noise Offset", float) = 1
        _Tess ("Tessellation", float) = 2
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 200

        CGPROGRAM

        #include "Packages/jp.keijiro.noiseshader/Shader/ClassicNoise3D.hlsl"
        
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert addshadow tessellate:tessFixed

        #pragma target 4.6

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        float3 _Point;
        float _AOE;
        float _NoiseRes;
        float _EffectScale;
        float _NoiseIntensification;
        float _UseTime;
        float _NoiseOffset;
        float _Tess;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
        // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float4 tessFixed()
        {
            return _Tess;
        }
        
        float3 clampMagnitude(float3 vec, float mag)
        {
            float len = length(vec);
            if(length(vec) < mag)
            {
                return vec;
            }

            return float3(
                (vec.x / len) * mag,
                (vec.y / len) * mag,
                (vec.z / len) * mag
            );
        }
        
        void vert(inout appdata_full v)
        {
            float3 normal_world_pos = UnityObjectToWorldDir(v.normal);
            float3 vertex_world_pos = mul(unity_ObjectToWorld, v.vertex);

            float3 dir_to_point = normalize(_Point - vertex_world_pos);

            //TODO: Use 4D noise and use xyz
            float noise = saturate(cnoise((float3(vertex_world_pos.xz, _NoiseOffset)) * _NoiseRes));

            float distance_to_point = distance(vertex_world_pos, _Point);
            
            float inverted_clamped_distance = 1 - saturate(distance_to_point);
            float intensified_noise = pow(inverted_clamped_distance * noise, _NoiseIntensification);

            float time = _UseTime * (0.5 + sin(_Time.y) * 0.5f);
            float aoe_clamped_dist = clamp(-1, 1, (_AOE - distance_to_point) / _AOE);
            float3 final_dir = lerp(0.4 * normal_world_pos, dir_to_point, time + aoe_clamped_dist);

            float3 effect = _EffectScale * final_dir * intensified_noise;
            
            vertex_world_pos += clampMagnitude(effect, distance_to_point);

            v.vertex = mul(unity_WorldToObject, float4(vertex_world_pos, 1));
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}