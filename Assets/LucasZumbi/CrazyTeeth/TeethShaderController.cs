﻿using UnityEngine;

namespace LucasZumbi.CrazyTeeth
{
    [ExecuteInEditMode]
    public class TeethShaderController : MonoBehaviour
    {
        public Transform target;
        public Renderer teeth;
        private MaterialPropertyBlock mpb;
        private static readonly int Point = Shader.PropertyToID("_Point");
        private static readonly int NoiseOffset = Shader.PropertyToID("_NoiseOffset");

        public float timescale = 0.1f;
        private float currTime;
        
        private void OnEnable()
        {
            mpb = new MaterialPropertyBlock();
        }

        private void Update()
        {
            if(!teeth) return;
            if(!target) return;

            currTime += Time.deltaTime * timescale;
            
            teeth.GetPropertyBlock(mpb);
            mpb.SetVector(Point, target.position);
            mpb.SetFloat(NoiseOffset, currTime);
            teeth.SetPropertyBlock(mpb);
        }
    }
}
